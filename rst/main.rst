.. raw:: html

   <!-- (php # 2 __DATE__ Finansielt system) -->
   <!-- ($attrib_AC=0;) -->

========================
Det finansielle systemet
========================

*Av: |author| / |datetime|*

.. include:: revision-header.rst

Jeg prøver å sette meg inn i et forholdsvis nytt og veldig innholdsrikt og interessant dokument fra Norges Bank:

* `Norges Bank, Det norske finansielle systemet, 2017
  <http://www.norges-bank.no/Publisert/Publikasjoner/det-norske-finansielle-systemet/2017/>`_

Dette henviser til en rekke andre dokumenter, og det er spesielt to av disse jeg har inkludert i 'studien':

* `Sigbjørn Atle Berg (Norges Bank), The declining deposit to loan ratio - What can the banks do?, 2012
  <http://www.norges-bank.no/en/Published/Papers/Staff-Memo/2012/Staff-Memo-282012/>`_
* `Henrik Borchgrevink, Ylva Søvik, Bent Vale (Norges Bank), Why regulate banks?, 2013
  <http://www.norges-bank.no/en/Published/Papers/Staff-Memo/2013/Staff-Memo-162013/>`_

Det jeg ønsker å ende opp med etter å ha satt meg godt nok inn i dette stoffet,
er å kunne ta stilling til hva jeg mener er problemet med det finansielle systemet.
Min magefølelse idag er at de strukturelle endringene som har skjedd de siste 30 til 40 årene
innenfor det finansielle systemet ikke er i allmenhetens interesse.
Spesielt interessant er det at tiltak som er ment som forbedringer muligens virker i stikk motsatt retning.
Jeg tenker da spesielt på overgangen til et pensjons-system basert på sparing i fond.
Dette tror jeg er en vesentlig årsak til at forholdet mellom pengemengden og kredittmengden har blitt så ugunstig,
slik som jeg belyser i  `bankvesenet <bankvesenet>`_, som jeg skrev før jeg visste om dokumentene som jeg refererer til her.

Jeg tror at økende ulikheter også har mye å si for utviklingen:
De rikeste ønsker i større grad å investere i "finansielle instrumenter".
Effekten av dette er at penger forsvinner ut at pengeholdene sektor, en effekt som Sigbørn Alte Berg kaller
lekkasje av penger (se sitater nedenfor).

I dokumentet til Berg går mye av plassen med til å diskutere om det det er "høna eller egget" som er forklaringen.
Høna representert ved hva kundene i pengeholdende sektor ser seg best tjent med, og egget representert ved
at det har skjedd en strukturell endring av det finansielle systemet de siste 30 til 40 årene.

Begge deler påvirker jo hverandre, og den finansielle strukturen er jo en kulturell greie som vi som samfunn
kan endre på "når som helst". Problemet er bare at dette stoffet er nokså innviklede saker som det
tar en viss tid å sette seg inn i. Jeg har på følelsen av at selv mange økonomer ikke nødvendigvis har full oversikt.

Folk flest må jo bare stole på at de som har definisjonsmakt her, som i stor grad er mest velstående blant oss
samt banksystemet selv,
gjør alt de kan for å skape et system som er i fellesskapets interesse.
Men hva om det ikke er slik? Hva om visse deler i alle fall av banksystemet samt en pengesterk lobby meler sin egen kake?
Den jevne person har ingen mulighet til å komme med motforestillinger mot et så komplekst og ekspert-styrt system.
Et aldri så lite demokratisk problem, synes jeg.

.. [//]: # (Politikere har vel også i liten grad mulighet til å gjøre stort annet enn å nikke og smile og late som at de forstår)
.. [//]: # (når bank-ekspertene og -lobbyistene forteller dem hvordan ting må være. Og tro meg, dette er ikke vondt ment,)
.. [//]: # (jeg tror dette gjelder selveste `finansministeren <https://twitter.com/Siv_Jensen_FrP>`_ også.)

`Arne Jon Isachsens <https://twitter.com/ajisachsen>`_ siste `månedsbrev <http://home.bi.no/fag87025/pdf/mb/201706.pdf>`_
gir et visst håp om at noe positivt tross alt er på gang i regi av ECB (Den europeiske sentralbanken):
Måten den Spanske banken *Banco Popular* blir avviklet på, og at tapet denne gangen for en stor del ble tatt av investorene,
ikke skattebetalerene.

Én ting jeg stadig vekk reflekterer over, er at samfunnet tilsynelatende beveger seg bort fra en tilstand der vi
lærer at vi må stole på
hverandre og mot en tilstand der vi for all del vil sikre oss selv.
Dette gjelder både nasjonalt og internasjonalt. Agendaen til Donald Trump går jo i høyeste grad i den retningen.
Nasjonalt tenker jeg igjen på pensjonsfond:
Vi blir lært opp til at vi ikke bør stole på at nåværende generasjon med arbeids-inntekt solidarisk sørger for at den eldre
ikke-arbeidende generasjonen får midler via skatte-omfordeling.
I stedet er det sparing i fond som gjelder.
Slike fond kan jo gå over ende, så i mitt hode synes jeg ikke dette virker helt trygt heller.
Hvis dette i tillegg er en sterkt medvirkende årsak til forgjeldingen av samfunnet,
så bør vi snarest mulig komme på en måte å forbedre systemet på!

Sitater fra hoveddokumentet
---------------------------

**"Når inntekten øker i et samfunn, er det en tendens til at både gjeld og finansielle fordringer øker enda mer.
I Norge er den samlede gjelden til privat sektor og kommunene (K3) målt som andel av verdiskapingen
mer enn tredoblet siden 1946"**

.. [//]: # (Forbrukslån er kredittkortgjeld og annen gjeld som ikke har pantesikkerhet.)

"De største utstederne i obligasjonsmarkedet er banker og kredittforetak, staten og bedrifter.
Blant de største investorgruppene finner vi livsforsikringsforetak og pensjonskasser, verdipapirfond og banker."

"Det har utenfor Norge også blitt noe mer vanlig med såkalte «ultralange» statsobligasjoner.
Disse kan ha løpetid på 40–100 år."

"OMF brukes i utstrakt grad av banksystemet for å finansiere boliglån"

"Vanligvis låner stater penger for å finansiere budsjettunderskudd og for å styrke valutareservene"

"De statlige utlånsordningene som finansieres gjennom opptak av statsgjeld
inkluderer blant annet Statens lånekasse for
utdanning, Husbanken, boliglånsordningen i Statens
pensjonskasse og Eksportkreditt Norge"

"Kommunene låner også mye gjennom Kommunalbanken,
som i hovedsak henter finansieringen i utenlandske obligasjonsmarkeder"

"I internasjonal sammenheng kjennetegnes det norske banksystemet av en
høy andel obligasjonsfinansiering"

"Livsforsikringsforetak og pensjonskasser har langsiktige forpliktelser og har tradisjonelt
investert i obligasjoner med lang løpetid og lav kredittrisiko. Disse
aktørene er derfor store investorer i statsobligasjoner,
OMF og kommuneobligasjoner, men de kjøper også
obligasjoner utstedt av bedrifter med lav kredittrisiko."

"Verdipapirfond
forvalter sparing på vegne av sine
kunder. Hva slags papirer det individuelle fondet
investerer i, henger sammen med hva slags spareprodukt de har solgt til kundene.
Enkelte fond investerer bare i statsobligasjoner, mens andre kjøper høyrisikoobligasjoner."

"Auksjon av statsobligasjonene sikrer lave lånekostnader for staten."

**"Valutamarkedet er målt etter omsetning verdens
største marked. (...)
Valutamarkedet
er blant markedene som er underlagt færrest reguleringer og myndighetskrav."**

"Banker har tradisjonelt hatt en viktig rolle som prisstillere i valutamarkedet, gjennom å stille bindende
kjøps- og salgskurser som aktørene kan handle til.
(...)
I dag
foregår en betydelig del av valutahandelen på elektroniske handelsplattformer. I senere år har også
andre aktører enn banker begynt å opptre som prisstillere i markedet. Dette gjelder for eksempel store
finansielle aktører som spesialfond (hedgefond) og
andre «High Frequency Traders» (HFT). Slike aktører
kan handle i store internasjonale bankers navn via
spesielle motpartskoder og på bankenes kredittlinjer.
Bankene tar seg betalt for å gi kredittlinjer. Slike
aktører vil ofte basere valutahandelen på såkalte algoritmer."

"Bankene er den største og viktigste gruppen finansforetak.
De har enerett til å ta imot innskudd fra allmennheten og står for størstedelen av utlånene."

"Også kredittforetakene kan låne ut penger,
men de kan ikke ta imot innskudd. De har tatt over
mye av boligfinansieringen etter at det i 2007 ble tillatt
å etablere en ny type kredittforetak som utsteder
obligasjoner med fortrinnsrett (OMF)."

"Forbrukslånsbankene tiltrekker seg innskudd gjennom å tilby markant høyere innskuddsrente enn andre banker.
kombinasjon av høy rente og innskudd som i stor grad er garantert av Bankenes sikringsfond
gjør forbrukslånsbanker attraktive for innskytere."

"Norske banker har en samlet forvaltningskapital på omtrent to ganger BNP"

"Reguleringer kan ha kostnader i form av lavere
produksjon av finansielle tjenester. Dersom regulering
gir gevinster som er høyere enn kostnadene ved regulering,
er det til nytte for samfunnet som helhet."

"Det foreslåtte kravet til stabil finansiering («Net Stable
Funding Ratio» eller NSFR) fra EU innebærer at
bankene finansierer lite likvide eiendeler med stabil
finansiering. Kravet skal gjøre bankenes finansieringsstruktur
mer robust. Utlån til kunder og pantsatte
eiendeler er eksempler på lite likvide eiendeler. Stabil
finansiering vil blant annet være kjernekapital, obligasjonsfinansering
med lang gjenværende løpetid og
flere typer kundeinnskudd. Kravet skal innføres to år
etter at det er endelig vedtatt."


Sitater fra dokumentet som omtaler bankenes mulighet til forbedre forholdet mellom innskudd og utlån
----------------------------------------------------------------------------------------------------

"Notice that alternative financial investments in
the portfolio model above will in many cases involve a payment to the financial sector and
thus represent a leakage in the loan-deposit chain."

There are two reasons for these deviations (...). First, the bank deposit chain has leakages
 whenever payments are made from the non-financial sector to any outside
sector. These outside sectors are the financial sector, the central
government and the foreign sector. Any investments of the non-financial
sector in financial
instruments other than bank deposits will involve payments to the financial sector. The
increased equity investment and insurance reserves shown in charts 5 and 6 represent such
leakages. These increased leakages have contributed to the decline of the deposit-to-loan
ratio."

"On the structural side, we first find that funded pension schemes have become more important
during the past 30 years"

**"We are often told that banks should increase
their deposit funding and reduce their reliance on
market funding, meaning that they should engineer a rise in
the deposit-to-loan ratio. While
this is clearly desirable from a financial stability point of view, it is
less clear how much the
banks can actually do. There is very little they can do with some of the structural changes that
have taken place."**

"Banks can restrain their sales of new financial
products of the kind that have shifted investments out of deposits
and into alternative financial
instruments, but that must a concerted effort
to make much difference at the macro level."

Sitater fra dokumentet som forklarer hvorfor bankvesenet må reguleres
---------------------------------------------------------------------

"Regulation is justified by market failures.
More precisely, regulation is beneficial when market outcomes are socially inefficient and regulation can
improve efficiency in a way that outweighs the costs of regulation."

**"(...) the banking system is inherently unstable,
for reasons to be explained in this note."**

"The instability of the banking system may lead to a smaller provision of bank
services than what is socially optimal.
Nevertheless, the safety nets that have been implemented to make banking more stable,
can easily lead to higher provision of bank services and more risk-taking
than what is optimal."

.. raw:: html

   <hr>
   <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Det%20finansielle%20systemet">Tweet</a>
   <br>
   <a target="_blank" href="show?f=articles/parsed/finansielt-system/nor/finansielt-system.pdf">som pdf</a>
